use rocket::http::Status;
use rocket_contrib::json::Json;
use rocket_contrib::json;
use rocket_contrib::json::JsonValue;
use rocket::request::Form;
use rocket::http::RawStr;
use sha2::{Sha256, Digest};
use sha2::digest::{DynDigest, Update};
use base64::encode;
use crate::connection::DbConn;
use crate::lib::structures::account;
use std::time::{SystemTime, UNIX_EPOCH};
use diesel::sql_types::VarChar;
use rocket::Data;
use std::net::SocketAddr;
use crate::lib::structures::account::Account;

#[derive(FromForm)]
pub struct RegistrationForm<'r> {
    pub handle: &'r RawStr,
    pub username: &'r RawStr,
    pub password: &'r RawStr,
    pub email: &'r RawStr
}

#[post("/register", data = "<form>")]
pub fn register(remote_addr: SocketAddr, connection: DbConn, form: Form<RegistrationForm>) -> Json<JsonValue> {
    let mut hasher = Sha256::new();
    DynDigest::update(&mut hasher, form.password.as_bytes());
    let result = hasher.finalize();

    let hash = encode(result);

    account::new_account(account::InsertableAccount {
        handle: form.handle.to_string(),
        username: form.username.to_string(),
        password_hash: hash,
        roles: String::from(""),
        register_time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs().to_string(),
        register_ip: String::from(remote_addr.ip().to_string()),
        ipv6: remote_addr.is_ipv6(),
        verified: false
    }, &connection);

    Json(json!({
        "code": 200,
        "message": "account created."
    }))
}

#[derive(FromForm)]
pub struct LoginForm<'r> {
    pub handle: &'r RawStr,
    pub password: &'r RawStr,
}

#[post("/login", data = "<form>")]
pub fn login(connection: DbConn, form: Form<LoginForm>) -> Json<JsonValue> {
    let mut hasher = Sha256::new();
    DynDigest::update(&mut hasher, form.password.as_bytes());
    let result = hasher.finalize();
    let hash = encode(result);

    let mut acc = account::search_by_handle(form.handle.to_string(), &connection);
    let queried_account = acc.ok().and_then(|mut a| a.pop());
    let queried_account = if let Some(q) = queried_account {
        q
    } else {
        return Json(json!({
            "code": 401,
            "message": "Username or password is incorrect."
        }))
    };

    if queried_account.password_hash != hash {
        return Json(json!({
            "code": 401,
            "message": "Username or password is incorrect."
        }))
    }

    Json(json!({
        "code": 200,
        "data": {
            "id": queried_account.id,
            "country": queried_account.country,
            "handle": queried_account.handle,
            "location": queried_account.location,
            "register_time": queried_account.register_time,
            "roles": queried_account.roles,
            "username": queried_account.username,
            "verified": queried_account.verified
        }
    }))
}