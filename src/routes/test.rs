use rocket::http::Status;
use rocket_contrib::json::Json;
use rocket_contrib::json;
use rocket_contrib::json::JsonValue;

#[get("/")]
pub fn get() -> Json<JsonValue> {
    Json(json!({
        "code": 200,
        "message": "hi there!"
    }))
}