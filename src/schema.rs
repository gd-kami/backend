table! {
    accounts (id) {
        id -> Integer,
        handle -> Text,
        username -> Text,
        password_hash -> Text,
        roles -> Text,
        country -> Nullable<Varchar>,
        location -> Nullable<Text>,
        register_time -> Varchar,
        register_ip -> Text,
        ipv6 -> Bool,
        verified -> Bool,
    }
}
