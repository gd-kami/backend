#![feature(decl_macro, proc_macro_hygiene)]
#[macro_use]
extern crate diesel;
extern crate toml;
extern crate r2d2;
extern crate r2d2_diesel;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate sha2;
#[macro_use]
extern crate base64;

use std::fs;

mod lib;

mod router;
mod connection;
mod routes;
pub mod schema;

#[derive(Deserialize)]
pub struct Config {
    pool: PoolConfig,
    ports: PortsConfig
}

#[derive(Deserialize)]
pub struct PoolConfig {
    host: String,
    user: String,
    password: String,
    database: String
}

#[derive(Deserialize)]
pub struct PortsConfig {
    rest_api: u16,
    tcp: u16
}

fn main() {
    let contents = fs::read_to_string("config.toml")
        .expect("Something went wrong reading the file");

    let config: Config = toml::from_str(&contents[..]).unwrap();
    router::create_routes(config);
}
