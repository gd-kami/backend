#![allow(proc_macro_derive_resolution_fallback)]

use crate::schema::accounts;
use diesel::{MysqlConnection, QueryResult, RunQueryDsl, QueryDsl, ExpressionMethods};
use diesel::sql_types::Integer;
use diesel::associations::HasTable;

#[derive(Queryable, AsChangeset, Serialize, Deserialize, Debug)]
pub struct Account {
    pub id: i32,
    pub handle: String,
    pub username: String,
    pub password_hash: String,
    pub roles: String,
    pub country: Option<String>,
    pub location: Option<String>,
    pub register_time: String,
    pub register_ip: String,
    pub ipv6: bool,
    pub verified: bool
}

#[derive(Insertable, Serialize, Deserialize)]
#[table_name="accounts"]
pub struct InsertableAccount {
    pub handle: String,
    pub username: String,
    pub password_hash: String,
    pub roles: String,
    pub register_time: String,
    pub register_ip: String,
    pub ipv6: bool,
    pub verified: bool,
}

pub fn new_account(data: InsertableAccount, conn: &MysqlConnection) -> QueryResult<usize> {
    use crate::schema::accounts::dsl::*;

    diesel::insert_into(accounts)
        .values(&data)
        .execute(conn)
}

pub fn search_by_id(account_id: i32, conn: &MysqlConnection) -> QueryResult<Vec<Account>> {
    use crate::schema::accounts::dsl::*;

    accounts.filter(id.eq(account_id))
        .load::<Account>(conn)
}

pub fn search_by_handle(account_handle: String, conn: &MysqlConnection) -> QueryResult<Vec<Account>> {
    use crate::schema::accounts::dsl::*;

    accounts.filter(handle.eq(account_handle))
        .load::<Account>(conn)
}