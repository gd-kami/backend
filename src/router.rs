use rocket;

use crate::connection;
use crate::routes;
use crate::Config;

pub fn create_routes(config: Config) {
    // Let's build our own config.
    let router_config = rocket::config::Config::build(rocket::config::Environment::Development)
        .address("127.0.0.1")
        .port(config.ports.rest_api)
        .unwrap();

    rocket::custom(router_config)
        .manage(connection::init_pool(config))
        // I probably want to have this set automatically... don't know how to do that though.
        .mount("/test",
            routes![
                routes::test::get
            ])
        .mount("/auth",
               routes![
                routes::authentication::register,
                routes::authentication::login
            ])
        .launch();
}