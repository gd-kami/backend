create table accounts
(
    id int auto_increment,
    handle text not null,
    username text not null,
    password_hash text not null,
    roles text not null,
    country varchar(2) default 'XX' null,
    location text null,
    register_time varchar(20) not null,
    register_ip text not null,
    ipv6 boolean not null,
    verified boolean not null,
    constraint accounts_pk
        primary key (id)
);